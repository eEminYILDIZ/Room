﻿using Room4net;
using System.IO;

namespace Room4net_Example
{
    public class RoomDB : RoomContext
    {
        public IntegratedList<Student> students = new IntegratedList<Student>();

        public RoomDB()
        {
            // For Microsoft Access:
            // string dbInfo = Directory.GetCurrentDirectory() + "/studentsDB.accdb";
            // For MsSQL Server:
            // string dbInfo = @"Data Source=FORDEV\SQLEXPRESS; Initial Catalog=studentsDB;Integrated Security=True;";
            // For SQLite:
            string dbInfo = Directory.GetCurrentDirectory() + "/studentsDB.sqlite";
            DatabaseTypes dbType = DatabaseTypes.SQLite;

            base.SetContext(dbType, dbInfo);
        }
    }
}
