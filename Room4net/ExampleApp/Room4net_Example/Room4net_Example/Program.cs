﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Room4net_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            
            RoomDB room = new RoomDB();
           
            // Example Data
            Student newStudent1 = new Student() { nameSurname = "Ahmet Alptekin", age = 21 };
            Student newStudent2 = new Student() { nameSurname = "Mehmet Talan", age = 22 };
            Student newStudent3 = new Student() { nameSurname = "Can Almaz", age = 22 };

            // CREATING:
            room.students.Add(newStudent1);
            room.students.Add(newStudent2);
            room.students.Add(newStudent3);
            room.SaveChangesToDB();

            Console.WriteLine("Created 3 Students");
            List<Student> students = room.students.List();
            WriteStudents(students);

            
            // UPDATING:
            int studentId = students[0].id;
            Student updatedStudent = room.students.List().Where(w => w.id == studentId).FirstOrDefault();
            updatedStudent.nameSurname = "Hasan Alptekin";
            room.students.Update(updatedStudent);
            room.SaveChangesToDB();

            Console.WriteLine("\n\n\nUpdated Student: id=" + studentId);
            students = room.students.List();
            WriteStudents(students);


            // DELETING:
            studentId = students[1].id;
            Student studentForDelete = room.students.List().Where(w => w.id == studentId).FirstOrDefault();
            room.students.Delete(studentForDelete);
            room.SaveChangesToDB();

            Console.WriteLine("\n\n\nDeleted Student: id=" + studentId);
            students = room.students.List();
            WriteStudents(students);

            
            // QUERY //
            // Run query on DB
            List<Student> matchedStudents1 = room.students.Equal(w => w.age, 22).AND().Equal(w => w.nameSurname, "Can Almaz").ExecuteForListing();
            // Run query on exist list
            List<Student> matchedStudents2 = room.students.List().Where(w => w.age == 22 && w.nameSurname == "Can Almaz").ToList();
            Console.WriteLine("\n\n\nQuery Results:");
            Console.WriteLine("DB Query Result: "+matchedStudents1[0].nameSurname);
            Console.WriteLine("List Query Rslt: "+matchedStudents2[0].nameSurname);


            // Clear student table: for next running this example app
            for (int i = 0; students.Count > 0; i++)
            {
                Student student = students[0];
                room.students.Delete(student);
            }
            room.SaveChangesToDB();


            // For holding console on the screen
            Console.Write("\nPress any key to close..."); ; Console.Read();
        }

        static void WriteStudents(List<Student> students)
        {
            Console.WriteLine("------------------------------");
            Console.WriteLine("Id\tName\t\tAge");
            Console.WriteLine("______________________________");
            foreach (Student student in students)
            {
                Console.WriteLine(student.id + "\t" + student.nameSurname + "\t" + student.age);
            }
            Console.WriteLine("------------------------------");

        }
    }
}
