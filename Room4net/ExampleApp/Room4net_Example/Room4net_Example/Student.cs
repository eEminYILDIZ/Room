﻿using Room4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Room4net_Example
{
    public class Student
    {

        [Room_Property_Attributes("Primary_Key", "Auto_Increment")]
        public int id { get; set; }

        public string nameSurname { get; set; }

        public int age { get; set; }
    }
}
