﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Room4net
{
    public class DbProcess_Access<T> : IDbProcess<T>
    {
        string connectionString = "";
        OleDbConnection connection = null;

        public DbProcess_Access(string dbFile)
        {
            // Veritabani/sibyan_takip_sistemi.accdb
            connectionString = @"Provider=Microsoft.ACE.Oledb.12.0;Data Source=" + dbFile;
            connection = new OleDbConnection(connectionString);
        }

        public void SetDBFile()
        {
        }

        // it will use when creating queries for target special characters
        DatabaseTypes dbType = DatabaseTypes.Access_07_10;
        
        //public static int Test()
        //{
        //    try
        //    {
        //        // string komut = "INSERT INTO donem(id,adi) VALUES(null,'test_donemi')"; // Error
        //        string komut = "INSERT INTO donem(adi) VALUES('test_donemi')";            // Not Error

        //        connection.Open();

        //        OleDbCommand command = new OleDbCommand(komut, connection);
        //        int sonuc = command.ExecuteNonQuery();

        //        connection.Close();

        //        if (sonuc == 1)
        //            return 1;
        //        else
        //            return 0;
        //    }
        //    catch
        //    {
        //        connection.Close();
        //        return 0;
        //    }
        //}

        public int Add(T p_object)
        {
            try
            {
                //Type object_type = typeof(T);
                Type object_type = p_object.GetType();

                string table_name = object_type.Name.ToLower();
                string values = "";
                string column_names = "";
                foreach (PropertyInfo property in object_type.GetProperties())
                {
                    string piece_of_string = "";
                    // if the propery is automatic number, write null to insert statement
                    if (Room_Property_Attributes.isPropertyHaveThisAttributes(property, Room_Property_Attributes.automatic_number_statement))
                    {
                        if (dbType == DatabaseTypes.MsSQL)
                            piece_of_string = "null";
                        else
                            continue;

                        // because the ms-access does not accept null statement for auto-increment number column
                        // but mssql allows.
                    }
                    else if (property.PropertyType == typeof(int))
                    {
                        int deger = Convert.ToInt32(property.GetValue(p_object,null));
                        piece_of_string = "" + deger;
                    }
                    else if (property.PropertyType == typeof(string))
                    {
                        string deger = property.GetValue(p_object, null).ToString();
                        piece_of_string = "'" + deger + "'";
                    }

                    column_names += property.Name + ",";
                    values += piece_of_string + ",";
                }

                column_names = column_names.Substring(0, column_names.Length - 1);
                values = values.Substring(0, values.Length - 1);

                string komut = "INSERT INTO " + table_name +
                    "(" +
                    column_names +
                    ")" +
                    " VALUES(" +
                    values +
                    ")";

                connection.Open();

                OleDbCommand command = new OleDbCommand(komut, connection);
                int sonuc = command.ExecuteNonQuery();

                connection.Close();

                if (sonuc == 1)
                    return 1;
                else
                    return 0;
            }
            catch
            {
                connection.Close();
                return 0;
            }
        }

        public int Delete(T p_object)
        {
            try
            {
                //Type object_type = typeof(T);
                Type object_type = p_object.GetType();

                string table_name = object_type.Name.ToLower();
                string where_statement = "";

                //foreach (PropertyInfo property in object_type.GetProperties())
                //{
                //    // if this property is primary_key:
                //    if (isPropertyHaveThisAttributes(property, primary_key_statement))
                //    {
                //        if (property.PropertyType == typeof(int))
                //        {
                //            int deger = Convert.ToInt32(property.GetValue(p_object));
                //            where_statement = property.Name + "=" + deger;
                //        }
                //        else if (property.PropertyType == typeof(string))
                //        {
                //            // if the property is a string, it have to be between the quotes
                //            string deger = property.GetValue(p_object).ToString();
                //            where_statement = property.Name + "=" + "'" + deger + "'";
                //        }
                //        break;
                //    }
                //}

                where_statement = Like_Parser<T>.Parse(p_object);

                 
                // protection for: not delete whole rows from table
                if (where_statement == "")
                    return -1;

                string komut = "DELETE FROM " + table_name +
                               " WHERE " + where_statement;

                connection.Open();

                OleDbCommand command = new OleDbCommand(komut, connection);
                int sonuc = command.ExecuteNonQuery();

                connection.Close();

                if (sonuc == 1)
                    return 1;
                else
                    return 0;
            }
            catch
            {
                connection.Close();

                return 0;
            }
        }

        // 
        public int Update(T p_object)
        {
            try
            {
                //Type object_type = typeof(T);
                Type object_type = p_object.GetType();

                string table_name = object_type.Name.ToLower();
                string update_statement = "";
                string where_statement = "";

                foreach (PropertyInfo property in object_type.GetProperties())
                {
                    string piece_of_string = "";
                    if (property.PropertyType == typeof(int))
                    {
                        if (property.GetValue(p_object, null) == (object)-1999999999)
                            continue;
                        
                        int deger = Convert.ToInt32(property.GetValue(p_object, null));
                        piece_of_string = property.Name + " = " + deger;
                    }
                    else if (property.PropertyType == typeof(string))
                    {
                        if (property.GetValue(p_object,null) == null)
                            continue;

                        string deger = property.GetValue(p_object, null).ToString();
                        piece_of_string = property.Name + " = " + "'" + deger + "'";
                    }

                    // if the property is primary key, store it for where statement
                    if (Room_Property_Attributes.isPropertyHaveThisAttributes(property, "Primary_Key"))
                        where_statement = piece_of_string; // this statement will override after 6 lines if the method have 2. parameter
                    else
                        update_statement += piece_of_string + ",";
                }

                update_statement = update_statement.Substring(0, update_statement.Length - 1);
                //if(p_where_object!=null)
                //    where_statement = Like_Parser<T>.Parse(p_where_object);

                string komut = "UPDATE " + table_name +
                    " SET " +
                    update_statement +
                    " WHERE " +
                    where_statement
                    ;

                connection.Open();

                OleDbCommand command = new OleDbCommand(komut, connection);
                int sonuc = command.ExecuteNonQuery();

                connection.Close();

                if (sonuc == 1)
                    return 1;
                else
                    return 0;
            }
            catch
            {
                connection.Close();
                return 0;
            }
        }

        public List<T> GetList(T p_object=default(T))
        {
            try
            {
                Type object_type = typeof(T);
                
                connection.Open();

                List<T> _return = new List<T>();

                // build where statement if parameter object is a filled object:
                string where_statement = Like_Parser<T>.Parse(p_object);

                string komut = "";
                if (p_object == null)
                {
                    komut = "SELECT * FROM " + object_type.Name.ToLower();
                }
                else
                {
                    if(where_statement!="")
                        komut = "SELECT * FROM " + object_type.Name.ToLower() + " WHERE " + where_statement;
                }

                OleDbCommand command = new OleDbCommand(komut, connection);
                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    T created_object = (T)Activator.CreateInstance(object_type);

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string colum_name = reader.GetName(i);

                        PropertyInfo property = object_type.GetProperty(colum_name);

                        if (property == null)
                            continue;

                        if (property.PropertyType == typeof(int))
                        {
                            property.SetValue(created_object, Convert.ToInt32(reader[colum_name]), null);
                        }
                        else if (property.PropertyType == typeof(string))
                        {
                            property.SetValue(created_object, reader[colum_name].ToString(), null);
                        }
                    }
                    _return.Add(created_object);
                }

                connection.Close();

                return _return;
            }
            catch
            {
                connection.Close();
                return null;
            }

        }


        public List<T> GetListWithQuery(string pWhere)
        {
            try
            {
                Type object_type = typeof(T);
                
                connection.Open();

                List<T> _return = new List<T>();

                string where_statement = pWhere;

                string komut = "SELECT * FROM " + object_type.Name.ToLower() + " WHERE " + where_statement;

                System.Data.Common.DbCommand command = connection.CreateCommand();
                command.CommandText = komut;
                System.Data.Common.DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    T created_object = (T)Activator.CreateInstance(object_type);

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string colum_name = reader.GetName(i);

                        PropertyInfo property = object_type.GetProperty(colum_name);

                        if (property.PropertyType == typeof(int))
                        {
                            property.SetValue(created_object, Convert.ToInt32(reader[colum_name]), null);
                        }
                        else if (property.PropertyType == typeof(string))
                        {
                            property.SetValue(created_object, reader[colum_name].ToString(), null);
                        }
                    }
                    _return.Add(created_object);
                }

                connection.Close();

                return _return;
            }
            catch
            {
                connection.Close();
                return null;
            }

        }









    }



}
