﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Room4net
{
    class Enums{}


    public enum DatabaseTypes
    {
        access_2003,
        Access_07_10,
        MsSQL,
        SQLite
    }

    public enum CompareType
    {
        Equal,
        NotEqual,
        Bigger,
        Smaller,
        BiggerOrEqual,
        SmallerOrEqual,
        LikeBefore,
        LikeAfter,
        Like,
    }

    public enum ConditionType
    {
        AND,
        OR,
        NULL
    }
}
