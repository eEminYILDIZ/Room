﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Room4net
{
    public partial class IntegratedList<T>
    {
        List<T> cached;

        List<T> will_add = new List<T>();
        List<T> will_update = new List<T>();
        List<T> will_delete = new List<T>();

        IDbProcess<T> DbProcess;

        public void Configure(DatabaseTypes dbType, string dbInfo)
        {
            IDbProcess<T> _dbProcess = null;


            switch (dbType)
            {
                case DatabaseTypes.Access_07_10:
                    _dbProcess = new DbProcess_Access<T>(dbInfo);
                    break;

                case DatabaseTypes.SQLite:
                    _dbProcess = new DbProcess_SQLite<T>(dbInfo);
                    break;

                case DatabaseTypes.MsSQL:
                    _dbProcess = new DbProcess_MsSQL<T>(dbInfo);
                    break;
            }

            DbProcess = _dbProcess;
        }

        public int SaveChangesToDB()
        {
            try
            {
                foreach (T obj in will_add)
                {
                    DbProcess.Add(obj);
                }

                foreach (T obj in will_delete)
                {
                    DbProcess.Delete(obj);
                }

                foreach (T obj in will_update)
                {
                    DbProcess.Update(obj);
                }

                will_add.Clear();
                will_update.Clear();
                will_delete.Clear();

                cached = null;

                return 1;
            }
            catch
            {
                return 0;
            }
        }


        public int Add(T obj)
        {
            try
            {
                will_add.Add(obj);
                cached.Add(obj);
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int Delete(T obj)
        {
            try
            {
                will_delete.Add(obj);
                cached.Remove(obj);
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int Update(T p_new)
        {
            try
            {
                will_update.Add( p_new );
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public List<T> ListFromDB(T p_object)
        {
            cached = DbProcess.GetList(p_object);
            return cached;
        }

        public List<T> List()
        {
            if (cached == null)
                cached = ListFromDB(default(T));

            return cached;
        }

    }
}
