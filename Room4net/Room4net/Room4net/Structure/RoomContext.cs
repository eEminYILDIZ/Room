﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Room4net
{
    public abstract class RoomContext
    {

        public void SetContext(DatabaseTypes dbType, string dbInfo)
        {
            Type object_type = this.GetType();
            FieldInfo[] properties = object_type.GetFields();

            foreach (FieldInfo property in properties)
            {
                Type fieldType = property.FieldType;

                Type dbModelIntegratedType = typeof(IntegratedList<object>);
                if (fieldType.Name == dbModelIntegratedType.Name)
                {
                    var dbo = property.GetValue(this);

                    Type type = dbo.GetType();
                    MethodInfo mi = type.GetMethod("Configure");
                    mi.Invoke(dbo, new object[] { dbType, dbInfo });
                }
            }
        }

        public bool SaveChangesToDB()
        {
            try
            {
                Type object_type = this.GetType();
                FieldInfo[] properties = object_type.GetFields();

                foreach (FieldInfo property in properties)
                {
                    Type t = property.FieldType;

                    Type t2 = typeof(IntegratedList<object>);
                    if (t.Name == t2.Name)
                    {
                        // Db_model_integrated<T>
                        var dbo = property.GetValue(this);

                        Type type = dbo.GetType();
                        MethodInfo mi = type.GetMethod("SaveChangesToDB");
                        mi.Invoke(dbo, null);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
