﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Room4net
{
    public partial class IntegratedList<T>
    {

        private string whereQueryString = "";


        // Condition Types:
        public IntegratedList<T> AND()
        {
            whereQueryString += " AND ";
            return this;
        }

        public IntegratedList<T> OR()
        {
            whereQueryString += " OR ";
            return this;
        }


        // Compare Types:

        public IntegratedList<T> Equal(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.Equal);
            return this;
        }

        public IntegratedList<T> NotEqual(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.NotEqual);
            return this;
        }

        public IntegratedList<T> Bigger(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.Bigger);
            return this;
        }

        public IntegratedList<T> Smaller(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.Smaller);
            return this;
        }

        public IntegratedList<T> BiggerEqual(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.BiggerOrEqual);
            return this;
        }

        public IntegratedList<T> SmallerEqual(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.SmallerOrEqual);
            return this;
        }

        public IntegratedList<T> Like(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.Like);
            return this;
        }

        public IntegratedList<T> LikeAfter(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.LikeAfter);
            return this;
        }

        public IntegratedList<T> LikeBefore(Expression<Func<T, object>> expression, object o)
        {
            ResolveExpression(expression, o, CompareType.LikeBefore);
            return this;
        }


        //------------------------------------------EXECUTEs--------------------------------------------------

        public List<T> ExecuteForListing()
        {
            List<T> _return = DbProcess.GetListWithQuery(whereQueryString);

            whereQueryString = "";

            return _return;
        }


        // Resolver:
        private void ResolveExpression(Expression<Func<T, object>> expression, object o, CompareType compareType)
        {
            string buildedWhereString = "";
            string compareString = "";


            switch (compareType)
            {
                case CompareType.Equal:
                    compareString = "=";
                    break;
                case CompareType.NotEqual:
                    compareString = "!=";
                    break;
                case CompareType.Bigger:
                    compareString = ">";
                    break;
                case CompareType.Smaller:
                    compareString = "<";
                    break;
                case CompareType.BiggerOrEqual:
                    compareString = ">=";
                    break;
                case CompareType.SmallerOrEqual:
                    compareString = "<=";
                    break;
                case CompareType.LikeBefore:
                    compareString = " Like ";
                    break;
                case CompareType.LikeAfter:
                    compareString = " Like ";
                    break;
                case CompareType.Like:
                    compareString = " Like ";
                    break;
            }


            PropertyInfo propertyInfo;

            if (expression.Body is UnaryExpression)
                propertyInfo = ((MemberExpression)((UnaryExpression)expression.Body).Operand).Member as PropertyInfo;
            else
                propertyInfo = ((MemberExpression)expression.Body).Member as PropertyInfo;

            if (propertyInfo == null)
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");

            string propertyName = propertyInfo.Name;
            object propertyValue = o.ToString();
            string propertyType = propertyInfo.GetGetMethod().ReturnType.Name; //String, Int, Double
            if (propertyType == "String")
            {
                if (compareType == CompareType.Like)
                    propertyValue = "'%" + propertyValue + "%'";
                else if (compareType == CompareType.LikeAfter)
                    propertyValue = "'%" + propertyValue + "'";
                else if (compareType == CompareType.LikeBefore)
                    propertyValue = "'" + propertyValue + "%'";
                else
                    propertyValue = "'" + propertyValue + "'";
            }

            buildedWhereString = propertyName + compareString + propertyValue;
            whereQueryString += buildedWhereString;
        }
    }
}
