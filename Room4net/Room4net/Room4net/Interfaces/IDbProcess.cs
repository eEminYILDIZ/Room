﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Room4net
{
    public interface IDbProcess<T>
    {
        int Add(T p_object);

        int Delete(T p_object);

        int Update(T p_object);

        List<T> GetList(T p_object = default(T));

        List<T> GetListWithQuery(string pWhere);
    }
}
