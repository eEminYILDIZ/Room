﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Room4net
{
    class Like_Parser<T>
    {
        public static string Parse(T _object)
        {
            if (_object == null)
                return "";

            PropertyInfo[] pinf = _object.GetType().GetProperties();

            string _return = "";
            foreach (PropertyInfo info in pinf)
            {
                object value = info.GetValue(_object, null);
                string name = info.Name;
                string val = "";

                if (info.PropertyType == typeof(Int32))
                {
                    if (Convert.ToInt32(value) == -1)
                        continue;
                    else
                        val = name + "=" + value;
                }
                else if (info.PropertyType == typeof(DateTime))
                {
                    if ((DateTime)value == (new DateTime()))
                        continue;
                    else
                        val = name + "='" + ((DateTime)value).ToShortDateString() + "'";
                }
                else// if (info.GetType() == typeof(string))
                {
                    if (value == null)
                        continue;
                    else
                        val = name + "='" + value + "'";
                }
                _return += val + " AND ";
            }

            // where statement can be empty becase object's all property can be default value
            if(_return.Length>5)
                _return = _return.Substring(0, _return.Length - 5);// deleting last AND statement

            return _return;
        }

    }
}
