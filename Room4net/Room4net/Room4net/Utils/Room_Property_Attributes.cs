﻿using System;
using System.Reflection;

namespace Room4net
{
    public class Room_Property_Attributes : Attribute
    {
        public string primary_key { get; set; }
        public string automatic_number { get; set; }
        public Room_Property_Attributes(string primary_key, string automatic_number)
        {
            this.primary_key = primary_key;
            this.automatic_number = automatic_number;
        }

        public static string primary_key_statement = "Primary_Key";
        public static string automatic_number_statement = "Auto_Increment";
        public static bool isPropertyHaveThisAttributes(PropertyInfo property, string attribute_name)
        {
            try
            {
                foreach (Room_Property_Attributes attribute in property.GetCustomAttributes(typeof(Room_Property_Attributes),false))
                {
                    if (attribute.primary_key == attribute_name || attribute.automatic_number == attribute_name)
                        return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
    }


}
